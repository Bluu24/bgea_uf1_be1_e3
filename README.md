 # **bgea_UF1_Be1_E3**
 UF1. Programació estructurada.
 <br><br>

## INDEX
- ### [1 Exercici-Missatge benvinguda](#ex-1-missatge-benvinguda)

- ### [2 Exercici-Tipus simples](#ex-2-tipus-simples)

- ### [3 Exercici-Suma enters](#ex-3-suma-enters)

- ### [4 Exercici-Tipus](#ex-4-tipus)

- ### [5 Exercici-Números](#ex-5-numeros)

- ### [6 Exercici-Suma](#ex-6-suma)

- ### [7 Exercici-Intercanvi](#ex-7-intercanvi)

- ### [8 Exercici-Pantalla de menú](#ex-8-pantalla-de-menu)

- ### [9 Exercici-Pantalla de menú](#ex-9-pantalla-de-menú)

- ### [10 Exercici-Càlcul del capital final en interès simple](#ex-10-calcul-del-capital-final-en-interes-simple)

- ### [11 Exercici-Càlcul del Capital final en interès compost](#ex-11-calcul-del-capital-final-en-interes-compost)

- ### [12 Exercici-La Travessa](#ex-12-la-travessa)

- ### [13 Exercici-Joc dels daus](#ex-13-joc-dels-daus)

<br>

## EX 1 Missatge benvinguda
 ---
Escriu un missatge de benvinguda per pantalla.
<div style="color:#4086ff"> </div>


```c
   printf("Hello ;) !");
```
![Missatge benvinguda](/imatges/Selecció_014.png)
 
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
<br>

## EX 2 Tipus simples
---
Escriu un enter, un real, i un caràcter per pantalla.

```c
    // Declaracio de variable
    int enter;
    double realDoblePrecisio;
    char caracter;

    // Inicialització
    enter=4;
    realDoblePrecisio=3.14;
    caracter='b';

    // Sortida per pantalla
    printf("\nEnter: %i \nDouble: %lf \nCaracter: %c\n" ,enter ,realDoblePrecisio ,caracter);
```

![Tipus simples](/imatges/Selecció_015.png)


<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
<br>

## EX 3 Suma enters
---
Demana dos enters per teclat i mostra la suma per pantalla.

```c
// Declaracio de variables
  int enter,enter2;

// Entrada per teclat i Sortida per pantalla
printf ("\nIntro 1r enter a sumeac :");
scanf  ("%i" ,&enter);                                       
printf ("Intro 2r enter a sumea.r :");
scanf  ("%i" ,&enter2);
printf (" \nEl resultat de %i + %i = %i \n" ,enter ,enter2 ,enter + enter2);
```
![Suma enters](/imatges/Selecció_016.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 4 Tipus
---
Indica el tipus de dades que correspon en cada cas:

* **Edat**-<div style="color:#4086ff"> Enter (int)</div>
* **Temperatura** -<div style="color:#4086ff"> Real (double)</div>
* **La resposta a la pregunta: Ets solter (s/n)?** -<div style="color:#4086ff"> Booleans (if)</div>
* **El resultat d’avaluar la següent expressió: (5>6 o 4<=8)** -<div style="color:#4086ff">Aritmètics</div>
* **El teu nom** -<div style="color:#4086ff"> Caracter (char)</div>

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 5 Numeros
---
Mostrar per pantalla els 5 primers nombres naturals:
```c
// Sortida a pantalla
printf ( "1,2,3,4,5\n" );
```
![Números](/imatges/Selecció_017.png)
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>
aritmètics.
printf ( "\nLa suma : %i ,el producte : %i\n" ,1+2+3+4+5 ,1*2*3*4*5);
```


![Suma](/imatges/Selecció_018.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 7 Intercanvi
---
Demana dos enters i intercanvia els valors de les variables:

```c
   //Variables
    int a,b;

  // Entrada per teclat i Sortida per pantalla
    printf ("\nIntro el 1r enter:");aritmètics.
```
![Intercanvi](/imatges/Selecció_020.png)

**INTERCANVI**
```c
//Variables
 int a,b,tmp;

  // Entrada per teclat i Sortida per pantalla
 printf ("\nIntro el 1r enter:");
 scanf  ("%i" ,&a);
 printf ("Intro el 2r enter:");
 scanf  ("%i",&b);

 tmp=a;
 a=b;
 b=tmp;

 printf ("\n Numero enter a = %i i Numero enter b = %i",a ,b);
```
![Missatge benvinguda](/imatges/Selecció_021.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 8 Pantalla de menu
---
Mostrar per pantalla el següent menú:
```c
//Variables
    int opcioMenu;

   //Imprimir menú:
   printf("Opcions\n");
   printf("\t1 - Alta\n");
   printf("\t2 - Baixar\n");
   printf("\t3 - Modificar\n");
   printf("\t4 - sortir\n");

   //Demanar el numero de més
   printf("\nTria opció (1-4):");
   scanf("%i",&opcioMenu);
```
![Pantalla de menú](/imatges/Selecció_022.png)
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 9 Pantalla de menu
---
Mostra per pantalla el següent menú:
```c
    //Variables
    int opcioMenu;

   //Imprimir menú:
   printf("********************\n");
   printf("\n***\tMENU\t***\n");
   printf("********************\n");
   printf("\n***\t1-alta\t***\n");
   printf("\n***\t2-baixa\t***\n");
   printf("\n***\t2-sorti\t***\n");
   printf("\n********************");
```
![Pantalla de menú](/imatges/Selecció_023.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 10 Calcul del capital final en interes simple
---
Feu un programa que resolgui el següent enunciat:

Calcular el Capital Final **CF** d’una inversió, a interès simples, si sabem  el Capital Inicial (**C0**), el nombre d’anys (**n**), i el rèdit (**r**). La fórmula per 
obtenir el CF a interès simples és:
```c
//Necesitam la llibreria #include <math.h>
   //Variables
 int C0 ,e ,t ,r ,k ,CF ,o;

 e=100;
 k=1;aritmètics.
 printf ("Numero anys k=%i\n",k);
 printf ("Numero en la formula e=%i\n",e);

 printf("\nCF=C0+((C0*r*t)/(100*k))\n");
 printf("\nFotmula emb dades CF=%i+((%i*%i*%i)/(%i*%i))\n",C0 ,C0 ,r ,t ,e ,k);

 CF=C0+((C0*r*t)/(e*k));
 o=CF;

 printf("\nCF=%i\n",o);

```

![Càlcul del capital final en interès simple](/imatges/Captura_026.png)
<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 11 Calcul del capital final en interes compost
Feu un programa que resolgui el següent enunciat:
Calcular el Capital Final CF d’una inversió, a interès compost, si sabem
el Capital Inicial (C0), el nombre d’anys (n), i el interès (i). La fórmula per obtenir el CF a interès compost és:
```c
//Tenim que agrewgar la llibreria #include <math.h>
    //Variables
 int C0 ,e ,n ,i ,CF ,o;

 e=1;

  // Entrada per teclat i Sortida per pantalla
 printf ("\nIntrodueix la Capital Inicial:");
 scanf  ("%i",&C0);
 printf ("Introdueix el i:");
 scanf  ("%i",&i);
 printf ("Introdueix n:");
 scanf  ("%i",&n);
 printf ("Numero en la formula e=%i\n",e);

 printf("\nCF=C0*(1+i)^n\n");
 printf("\nFotmula emb dades CF=%i*(%i+%i)^%i\n",C0 ,e ,i ,n);
aritmètics.

 printf("\nCF=%i\n",o);
```

![Càlcul del Capital final en interès compost](/imatges/Captura_024.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>

## EX 12 La Travessa
---
Feu un programa que permeti generar una travessa de forma aleatòria. 

Tot seguit ha de permetre introduir-ne una per teclat i comprovar-ne els encerts respecte la travessa generada aleatòriament. El format de sortida queda a la vostra elecció sempre que n’informi dels encerts.
Follow

## EX 13 Joc dels daus
---
Feu un programa que permeti simular el resultat de tirar un dau.
Tot seguit ha de demanar un número per teclat, entre 1 i 6, i ens ha dir si és el mateix que el que hem generat aleatòriament

```c
//Tenim que agregar la llibraruia #include <unistd.h>
   int aleatori;
    int entradaTeclat;

    srand(getpid());

    aleatori=rand()%6+1;


    printf("Escull un numero del dau:");
    scanf("%i",&entradaTeclat);

    if(entradaTeclat==aleatori){
            printf("\nL'has endivinat!!!;)\n");
    }else{
        printf("\nEl numero del deau era el: %i \nSegueix intentant-ho :( \n", aleatori);
        }


```
<br>
No encertat:

![Joc dels daus](/imatges/Captura_027.png)


Encertat:

![Joc dels daus](/imatges/Captura_028.png)

<div style='text-align: right'>

 [Tornar a l'INDEX...](#index)
</div>



